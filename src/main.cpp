#include <direct.h>
#include <vector>
#include <string>
#include <stdlib.h>
#include <time.h>
#include <algorithm>
#include <fstream>

std::vector<std::string> folderNames = { "BLACK", "GRIM", "DARKNESS", "EVIL", "PANIC", "CRY", "QUIT", "SCREAM", "UNFAIR", "ESCAPE", "HELP", "MISERY", "HELPLESS", "GIVE_UP", "TRAPPED", "LOCKED", "RED", "HOPELESS", "ANXIOUS", "DEAD_END", "ALONE", "COLD", "HELL", "GRAY", "UNLIT", "SOB", "SOMBER", "UNHAPPY", "NOBODY", "NOTHING", "NOWHERE", "NEVER", "NONE", "DON'T", "LOST", "DEEPER", "CANCEL", "STOP", "SOS", "VOID", "DOOM", "TIME_ENDS", "SHADOW", "SATAN", "DREAD", "FEAR", "RUN", "DAUNTING", "NEVERENDING", "ENDLESS", "ABYSS", "DEEP", "GRAVE", "TRAP", "CLOSED", "VOICELESS", "NUMB", "SILENT", "WORSE", "WORST", "UNDER", "OUT", "WEAK", "SOUL" };

typedef struct Folder
{
    std::string folderPath = "";
    Folder* parentFolder = nullptr;
    std::vector<Folder*> subfolders = {};
    int initedSubfolderIndex = 0;
} Folder;

int RandomRange(int min, int max)
{
    return rand() % (max - min + 1) + min;
}

void GenerateLevel()
{
    Folder* currentFolder = new Folder();
    currentFolder->folderPath = "HAVE_FUN/";
    _mkdir(currentFolder->folderPath.c_str());

    int numOfFolders = 0;
    int folderDepth = 0;
    std::vector<Folder*> fileFolders = {};

    bool directoryDone = false;
    while (!directoryDone)
    {
        int folderAmount = 0;
        if (folderDepth == 0)
        {
            folderAmount = 2;
        }
        else if (folderDepth <= 2)
        {
            folderAmount = RandomRange(2, 4);
        }
        else if (folderDepth == 3)
        {
            folderAmount = RandomRange(0, 2);
        }

        if (folderAmount == 0)
        {
            fileFolders.push_back(currentFolder);
        }

        for (int i = 0; i < folderAmount; ++i)
        {
            Folder* newFolder = new Folder();
            newFolder->folderPath = currentFolder->folderPath + folderNames[++numOfFolders] + '/';
            newFolder->parentFolder = currentFolder;
            currentFolder->subfolders.push_back(newFolder);
            _mkdir(newFolder->folderPath.c_str());
        }

        bool foundNextFolder = false;
        while (!foundNextFolder)
        {
            if (currentFolder->initedSubfolderIndex >= currentFolder->subfolders.size())
            {
                if (currentFolder->parentFolder == nullptr)
                {
                    directoryDone = true;
                    break;
                }
                else
                {
                    currentFolder = currentFolder->parentFolder;
                    --folderDepth;
                }
            }
            else
            {
                currentFolder = currentFolder->subfolders[currentFolder->initedSubfolderIndex++];
                foundNextFolder = true;
                ++folderDepth;
            }
        }
    }

    std::ofstream keyFile(fileFolders[RandomRange(0, fileFolders.size() - 1)]->folderPath + ".txt");
    keyFile << "ebin you found it yay";
    keyFile.close();
}

int main(int argc, char* argv[])
{
    srand(time(NULL));
    std::random_shuffle(folderNames.begin(), folderNames.end());
    
    GenerateLevel();

    return 0;
}
